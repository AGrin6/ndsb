import sys
import os
import glob
import random

import numpy
import theano
import theano.tensor as T

sys.path.append('/home/agrin/Install/grinlib')
from layer import LeNetConvPoolLayer
from layer import CudaConvnetLayer
from layer import HiddenLayer
from regression import LogisticRegression
from augmentation import get_deformed_data
from augmentation import get_reflected_data
from augmentation import get_reflected_shifted_data

from load_data import load_data
from load_data import load_test_data

from constants import OUTPUT_HEADER
from constants import COLOR_BOUNDARY

from constants import INPUT_CHANNELS
from constants import AUGMENTATION
from constants import RANDOM_SEED
from constants import BATCH_SIZE
from constants import LEARNING_RATE
from constants import LEARNING_RATE_DECAY
from constants import NUMBER_OF_EPOCHS

from constants import FIRST_LAYER_INPUT_SHAPE
from constants import FIRST_LAYER_MAPS_NUMBER
from constants import FIRST_LAYER_KERNEL_SHAPE
from constants import FIRST_LAYER_POOL_SHAPE
from constants import SECOND_LAYER_INPUT_SHAPE
from constants import SECOND_LAYER_MAPS_NUMBER
from constants import SECOND_LAYER_KERNEL_SHAPE
from constants import SECOND_LAYER_POOL_SHAPE
from constants import THIRD_LAYER_INPUT_SIZE
from constants import THIRD_LAYER_OUTPUT_SIZE
# from constants import FOURTH_LAYER_INPUT_SIZE
# from constants import FOURTH_LAYER_OUTPUT_SIZE
from constants import OUTPUT_LAYER_SIZE

# theano.config.exception_verbosity='high'

def write_log(log_output):
  log_counter = 0
  while os.path.exists('./data/log/experiment' + str(log_counter) + '.txt'):
    log_counter += 1

  with open('./data/log/experiment' + str(log_counter) + '.txt', 'w') as output_stream:
    output_stream.write(log_output)


def ReLU(x):
  return theano.tensor.switch(x < 0, 0, x)

def train_network():
  random.seed(0)

  datasets = load_data()
  shared_train_samples, shared_train_labels = datasets[0]
  shared_validation_samples, shared_validation_labels = datasets[1]
  print('Loading data completed')

  random_number_generator = numpy.random.RandomState(RANDOM_SEED)
  minibatch_index_var = T.lscalar('minibatch_index_var')
  network_input_var = T.matrix('network_input_var')
  network_output_var = T.ivector('network_output_var')

  first_layer_input = network_input_var.reshape((BATCH_SIZE, INPUT_CHANNELS,
                                                 FIRST_LAYER_INPUT_SHAPE[0],
                                                 FIRST_LAYER_INPUT_SHAPE[1]))

  # first_layer = LeNetConvPoolLayer(
  first_layer = CudaConvnetLayer(
                      random_number_generator,
                      symbolic_input=first_layer_input,
                      image_shape=(BATCH_SIZE, INPUT_CHANNELS) + FIRST_LAYER_INPUT_SHAPE,
                      filter_shape=(FIRST_LAYER_MAPS_NUMBER, INPUT_CHANNELS)
                                   + FIRST_LAYER_KERNEL_SHAPE,
                      poolsize=FIRST_LAYER_POOL_SHAPE,
                      activation=ReLU,
                      dropout_rate=0.5)

  # second_layer = LeNetConvPoolLayer(
  second_layer = CudaConvnetLayer(
                      random_number_generator,
                      symbolic_input=None,
                      image_shape=(BATCH_SIZE, FIRST_LAYER_MAPS_NUMBER) + SECOND_LAYER_INPUT_SHAPE,
                      filter_shape=((SECOND_LAYER_MAPS_NUMBER, FIRST_LAYER_MAPS_NUMBER)
                                   + SECOND_LAYER_KERNEL_SHAPE),
                      poolsize=SECOND_LAYER_POOL_SHAPE,
                      activation=ReLU,
                      dropout_rate=0.5,
                      previous_layer=first_layer)

  third_layer = HiddenLayer(random_number_generator,
                            symbolic_input=None,
                            number_of_inputs=SECOND_LAYER_MAPS_NUMBER * THIRD_LAYER_INPUT_SIZE,
                            number_of_outputs=THIRD_LAYER_OUTPUT_SIZE,
                            activation=ReLU,
                            dropout_rate=0.5,
                            previous_layer=second_layer
                            )

  # fourth_layer = HiddenLayer(random_number_generator,
  #                            symbolic_input=None,
  #                            number_of_inputs=THIRD_LAYER_OUTPUT_SIZE,
  #                            number_of_outputs=FOURTH_LAYER_OUTPUT_SIZE,
  #                            activation=ReLU,
  #                            dropout_rate=0.5,
  #                            previous_layer=third_layer)

  output_layer = LogisticRegression(symbolic_input=None,
                                    number_of_inputs=THIRD_LAYER_OUTPUT_SIZE,
                                    number_of_outputs=OUTPUT_LAYER_SIZE,
                                    previous_layer=third_layer)

  # output_layer = LogisticRegression(symbolic_input=None,
  #                                   number_of_inputs=FOURTH_LAYER_OUTPUT_SIZE,
  #                                   number_of_outputs=OUTPUT_LAYER_SIZE,
  #                                   previous_layer=fourth_layer)

  train_cost = output_layer.negative_log_likelihood(network_output_var, is_final=False)
  validation_cost = output_layer.negative_log_likelihood(network_output_var, is_final=True)
  parameters = (output_layer.parameters # + fourth_layer.parameters
                + third_layer.parameters
                + second_layer.parameters + first_layer.parameters)
  gradients = T.grad(train_cost, parameters)

  updates = []
  for parameter, gradient in zip(parameters, gradients):
    updates.append((parameter, parameter - LEARNING_RATE * gradient))

  print('Compiling models')
  validation_model = theano.function(
     [minibatch_index_var],
     validation_cost,
     givens={
       network_input_var: shared_validation_samples[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE],
       network_output_var: T.cast(shared_validation_labels[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE], 'int32')})

  train_model = theano.function(
     [minibatch_index_var],
     train_cost,
     updates=updates,
     givens={
       network_input_var: shared_train_samples[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE],
       network_output_var: T.cast(shared_train_labels[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE], 'int32')})

  print('Training started')
  train_batches_number = (
             shared_train_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)
  validation_batches_number = (
        shared_validation_samples.get_value(borrow=True).shape[0] / BATCH_SIZE)

  current_learning_rate = LEARNING_RATE
  list_of_log_lines = []

  for epoch_counter in xrange(NUMBER_OF_EPOCHS):
    current_learning_rate *= LEARNING_RATE_DECAY
    updates = []
    for parameter, gradient in zip(parameters, gradients):
      updates.append((parameter, parameter - current_learning_rate * gradient))

    if AUGMENTATION:
      # rotation_angle = random.choice([0, 90, 180, 270])
      # shifted_train_samples = theano.shared(
      #                             get_deformed_data(shared_train_samples.get_value(borrow=True),
      #                                               rotation_angle=rotation_angle,
      #                                               original_shape=FIRST_LAYER_INPUT_SHAPE,
      #                                               fill=1.))

      if epoch_counter < 400:
      # if True:
        reflection_x = random.choice([False, True])
        reflection_y = random.choice([False, True])
        transposition = random.choice([False, True])
        vertical_shift = random.randint(-2, 2)
        horizontal_shift = random.randint(-2, 2)
        # reflection_x = False
        # reflection_y = False
        # transposition = False
        # vertical_shift = 0
        # horizontal_shift = 0
      else:
        reflection_x = False
        reflection_y = False
        transposition = False
        vertical_shift = 0
        horizontal_shift = 0
      
      shifted_train_samples = theano.shared(
                           get_reflected_shifted_data(shared_train_samples.get_value(borrow=True),
                                                      original_shape=FIRST_LAYER_INPUT_SHAPE,
                                                      reflection_x=reflection_x,
                                                      reflection_y=reflection_y,
                                                      transposition=transposition,
                                                      vertical_shift=vertical_shift,
                                                      horizontal_shift=horizontal_shift))

      shifted_train_model = theano.function([minibatch_index_var], train_cost, updates=updates,
         givens={network_input_var: shifted_train_samples[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE],
                 network_output_var: T.cast(shared_train_labels[
                                minibatch_index_var * BATCH_SIZE:
                                (minibatch_index_var + 1) * BATCH_SIZE], 'int32')})

    train_losses = []
    for minibatch_index in xrange(train_batches_number):
      if AUGMENTATION:
        current_cost = shifted_train_model(minibatch_index)
        train_losses.append(current_cost)
      else:
        current_cost = train_model(minibatch_index)
        train_losses.append(current_cost)
      if minibatch_index == train_batches_number - 1:
        current_train_loss = numpy.mean(train_losses)
        if True:
          validation_losses = [validation_model(i) for i in xrange(validation_batches_number)]
          current_validation_loss = numpy.mean(validation_losses)
        new_log_line = 'Epoch: {} Current learning rate: {}'.format(
                                                              epoch_counter, current_learning_rate)
        list_of_log_lines.append(new_log_line)
        print(new_log_line)
        if AUGMENTATION:

          new_log_line = 'Reflection x: {} Reflection y: {} Transposition: {}'.format(
                                              reflection_x, reflection_y, transposition)
          list_of_log_lines.append(new_log_line)
          print(new_log_line)

          new_log_line = 'Vertical shift x: {} Horizontal shift y: {}'.format(
                                                  vertical_shift, horizontal_shift)
          list_of_log_lines.append(new_log_line)
          print(new_log_line)

        if True:
          new_log_line = 'Train error: {} Validation error: {}'.format(
                                                      current_train_loss, current_validation_loss)
        else:
          new_log_line = 'Train error: {}'.format(current_train_loss)
        list_of_log_lines.append(new_log_line)
        print(new_log_line)


  print('')
  print('Prediction started')
  shared_test_samples = load_test_data()

  prediction_model = theano.function(
                        [minibatch_index_var], output_layer.p_y_given_x_(is_final=True),
                        givens={network_input_var:
                                   shared_test_samples[minibatch_index_var * BATCH_SIZE:
                                                       (minibatch_index_var + 1) * BATCH_SIZE]})
  result = []
  test_batches_number = shared_test_samples.get_value().shape[0] / BATCH_SIZE
  for i in xrange(test_batches_number):
    new_prediction = prediction_model(i)
    result.extend(list(new_prediction))

  output_counter = 0
  while os.path.isfile('./data/outputs/output' + str(output_counter) + '.csv'):
    output_counter += 1

  list_of_test_files = glob.glob('./data/test/*')
  list_of_test_files.sort()
  with open('./data/outputs/output' + str(output_counter) + '.csv', 'w') as output_stream:
    output_stream.write(OUTPUT_HEADER + '\n')
    for prediction, test_file_name in zip(result, list_of_test_files):
      output_stream.write(os.path.split(test_file_name)[1])
      for value in prediction:
        output_stream.write(',' + str(value))
      output_stream.write('\n')
    
  print('Prediction completed')


  write_log('\n'.join(list_of_log_lines))

if __name__ == '__main__':
  train_network()
