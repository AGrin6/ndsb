import sys
import glob
from pprint import pprint

import numpy
from PIL import Image

import theano
import theano.tensor as T

import matplotlib.pyplot as plotter
import matplotlib.cm as colormap

sys.path.append('/home/agrin/Install/grinlib')
from data_preparation import complete_to_square
from data_preparation import shuffle_samples_labels

TRAIN_DATA_PATH = './data/train'
TEST_DATA_PATH = './data/test'

from constants import COLOR_BOUNDARY
from constants import FIRST_LAYER_INPUT_SHAPE
from constants import OUTPUT_HEADER

def load_data():
  list_of_samples = []
  list_of_labels = []

  labels_counter = 0
  # list_of_dirs = glob.glob(TRAIN_DATA_PATH + '/*')
  # list_of_dirs.sort()
  list_of_dirs = OUTPUT_HEADER.split(',')[1:]
  for dir_name in list_of_dirs:
    print(dir_name)
    # list_of_files = glob.glob(dir_name + '/' + '*')
    list_of_files = glob.glob(TRAIN_DATA_PATH + '/' + dir_name + '/' + '*')
    list_of_files.sort()
    for file_name in list_of_files:
      new_image = numpy.asarray(Image.open(file_name))
      new_image = complete_to_square(new_image, intensity=255)
      new_image = numpy.asarray(Image.fromarray(new_image).resize(FIRST_LAYER_INPUT_SHAPE,
                                                                  Image.ANTIALIAS))
      # plotter.imshow(new_image, cmap=colormap.Greys_r, interpolation='none')
      # plotter.show()
      list_of_samples.append(numpy.asarray(new_image.reshape(-1), dtype=float) / COLOR_BOUNDARY)
      list_of_labels.append(labels_counter)
      
    labels_counter += 1

  print('')

  list_of_samples, list_of_labels = shuffle_samples_labels(list_of_samples, list_of_labels, 1)

  number_of_test_samples = 5000

  shared_train_samples = theano.shared(numpy.array(list_of_samples[:-number_of_test_samples],
                                                   dtype=theano.config.floatX))
  shared_train_labels = theano.shared(numpy.array(list_of_labels[:-number_of_test_samples],
                                                 dtype=theano.config.floatX))
  shared_test_samples = theano.shared(numpy.array(list_of_samples[-number_of_test_samples:],
                                                  dtype=theano.config.floatX))
  shared_test_labels = theano.shared(numpy.array(list_of_labels[-number_of_test_samples:],
                                                 dtype=theano.config.floatX))

  return [(shared_train_samples, shared_train_labels),
          (shared_test_samples, shared_test_labels)]


def load_test_data():
  list_of_samples = []
  list_of_files = glob.glob(TEST_DATA_PATH + '/*')
  list_of_files.sort()
  for file_name in list_of_files:
    new_image = numpy.asarray(Image.open(file_name))
    new_image = complete_to_square(new_image, intensity=255)
    new_image = numpy.asarray(Image.fromarray(new_image).resize(FIRST_LAYER_INPUT_SHAPE,
                                                                Image.ANTIALIAS))
    # plotter.imshow(new_image, cmap=colormap.Greys_r, interpolation='none')
    # plotter.show()
    list_of_samples.append(numpy.asarray(new_image.reshape(-1), dtype=float) / COLOR_BOUNDARY)

  shared_test_samples = theano.shared(numpy.array(list_of_samples, dtype=theano.config.floatX))
  return shared_test_samples


if __name__ == '__main__':
  load_data()

